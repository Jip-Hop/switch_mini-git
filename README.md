# **Switch mini** #
Forked mlv_dump functions into a smaller more reliable, faster app.

**Switch mini**

![Screen_Shot_2017-07-26_at_10.05.56.png](https://i.postimg.cc/CK4K6Jf9/1.jpg)

![Screen_Shot_2017-07-26_at_10.05.56.png](https://i.postimg.cc/nLxzkNMv/2.jpg)

## HOWTO ##

- Check images

**regarding gatekeeper**

To supress gatekeeper hold ctrl or cmd button down(macOS Sierrra, Mojave, Catalina) while right clicking/opening the application the first time. You can also change permissions from within privacy/security settings.

**Thanks to:** a1ex, g3gg0, bouncyball, dmilligan, Dave Coffin(dcraw), Phil Harvey(Exiftool), Andreas Huggel(exiv2), dfort(Focus pixel script), Jip-Hop
#Copyright Danne
